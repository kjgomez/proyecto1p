#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

void *remover(hashtable *tabla, char *clave){
	int index= (long int)((hash((unsigned char *)clave))%(tabla->numeroBuckets)); //Se busca el índice con hashing.
	objeto *o=(tabla->buckets)[index]; //Se obtiene el bucket con índice obtenido.
	if(o==NULL){
		return NULL;
	}//Si no hay objeto en el bucket, retorna NULL.
	if(strcmp(o->clave,clave)==0){
		void *temp= o->valor;
		(tabla->buckets)[index]=o->siguiente;
		free(o);
		tabla->elementos--;
		return temp;
	} //Se maneja el primer caso, cuando el primer objeto del bucket tiene la clave del objeto que se quiere eliminar. En este caso, ahora el primero pasaría a ser el siguiente en la lista de objetos. Si es el único del bucket, el siguiente es NULL lo cual está correcto.
	else{
		while(o->siguiente!=NULL){
			if(strcmp(o->siguiente->clave,clave)==0){
				objeto *temp= o->siguiente;
				void *v= temp->valor;
				o->siguiente= o->siguiente->siguiente;
				free(temp);
				tabla->elementos--;
				return v; //Si se cumple, se remueve el elemento, y el siguiente del elemento anterior se encadena al siguiente del elemento eliminado.
			}
			o=o->siguiente;
		}
	}//El segundo caso es cuando es del segundo en adelante. Mientras el siguiente del objeto apuntado no es NULL, se comparará si es la clave a borrar. Si no lo es, se pasa a apuntar al objeto siguiente. El while termina cuando el siguiente del elemento apuntado es NULL. 
	return NULL;
}

