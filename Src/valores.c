#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

void **valores(hashtable *tabla, int *conteo){
	void **valores=malloc((tabla->elementos)*sizeof(void *));//Se inicializa el arreglo de puntero a valores
	int cont=0;
	int i;
	for(i=0;i<(tabla->numeroBuckets);i++){	
		objeto *obj=(tabla->buckets)[i];
		while(obj!=NULL){		//Se recorre cada elemento de cada bucket y se obtiene su valor agregandolo al arreglo de valores
			valores[cont]=obj->valor;
			obj=obj->siguiente;
			cont++;
		}
	}
	*conteo= cont;
	return valores;


}
