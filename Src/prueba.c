/**
 * Autor: Eduardo Murillo, M.Sc.
 * 
 **/
#include "hashtable.h"

//#define DEBUG

#define LINE_SIZE 256
#define CONTROL_ARRAY_SIZE 2		

char **claves_arr;
char **valores_arr;
int num_elementos_arr;
int TEST_ITEMS = 0;


void mostrarClavesValoresControl(){
	int i = 0;
	printf("\n\nArreglo de control:\n");
	for(i = 0; i < num_elementos_arr; i++){
		printf("%s : %s\n", claves_arr[i], valores_arr[i] );
	}
	printf("\n\n");
}



int verificarExistenciaClaves(char **lista_claves, int cant){

	int match = 0;

	int i = 0;
	for( i = 0; i < cant; i++){

		char *clave = lista_claves[i];

		int j = 0;
		for(j = 0; j < num_elementos_arr; j++){
			if(strcmp(claves_arr[j],clave) == 0){
				match++;
			}
		}

	}

	if(match == num_elementos_arr){
		return 0;
	}
	else if(match > num_elementos_arr){
		return 1;
	}
	else return -1;

}

int verificarExistenciaValores(char **lista_valores, char **lista_claves, int cant){

	int match = 0;

	int i = 0;
	for( i = 0; i < cant; i++){

		char *valor = lista_valores[i];
		char *clave = lista_claves[i];

		int j = 0;
		for(j = 0; j < num_elementos_arr; j++){
			if(strcmp(valores_arr[j],valor) == 0 && strcmp(claves_arr[j], clave) == 0){
				match++;
			}
		}

	}

	if(match == num_elementos_arr){
		return 0;
	}
	else if(match > num_elementos_arr){
		return 1;
	}
	else return -1;

}
int ar(char buf[], int b){
	return 0;
}

int numeroAzar(int ini, int fin){
	int semilla = time(NULL);
    srand(semilla);
	int numero = rand() % ( (fin-1) + 1 - ini) + 0;
	return numero;
}


void asignarValorControl(char *clave, char *valor){
	int i = 0;
	for(i = 0; i < num_elementos_arr; i++){
		if(strcmp(clave, claves_arr[i]) == 0){
			//Ya existe clave, actualizar
			valores_arr[i] = strdup(valor);
			return;
		}
	}

	//Si llegamos aqui la clave no existe;
	claves_arr[i] = strdup(clave);
	valores_arr[i] = strdup(valor);
	num_elementos_arr++;
}


void reemplazarCarater(char *str, char viejo, char nuevo){
	//Funcion insegura...No usarla en la practica.
	while(*str != 0){
		if(*str == viejo){
			*str = nuevo;
			return;
		}
		str++;
	}
}

int* numerosAzar(int inicio, int fin, int conteo){

	
	if(fin < conteo){
		printf("ERROR: El numero de elementos unicos del archivo de prueba (%d) debe ser mayor a la cantidad de elementos de prueba (%d). El programa se cerrara.\n", 
				fin, conteo);
		exit(-1);
	}
	int repetido = 0;
	int semilla = time(NULL);
    srand(semilla);
#ifdef DEBUG
	printf("conteo %d\n", conteo);
#endif

	int * nums = malloc(sizeof(int) * conteo);
	memset(nums,0, sizeof(int) * conteo	);
	int llenos = 0;
	int idx_actual = 0;
	while(llenos != conteo){	
		repetido = 0;
		int numero = rand() % ( (fin-1) + 1 - inicio) + 0;
		nums[idx_actual] = numero;
		llenos++;
		int i = 0;
		for(i = 0; i < llenos - 1;  i++){
#ifdef DEBUG
			printf("%d\n", nums[i]);
#endif
			if(nums[i] == nums[idx_actual]){
				llenos--;				//si hay numero repetido, volvemos a intenta
				repetido = 1;
				break;
			}
		}

		if(!repetido){
			idx_actual++;
		}
#ifdef DEBUG
		printf("\n");
#endif
	}
	return nums;

}

void llenarTabla(hashtable *tabla, char *archivo){

	FILE *file = fopen(archivo, "r");

	int i = 0;
	int block_multiple = 2;

	char buf[LINE_SIZE] = {0};
	if(file != NULL){
		//printf("Empezamos a leer el archivo...\n");
		while(fgets(buf, LINE_SIZE, file) != NULL){

			if(i != 0 && i % CONTROL_ARRAY_SIZE == 0){
#ifdef DEBUG
				printf("Incrementando tamano de arreglo de control...\n");
#endif
				claves_arr = realloc(claves_arr, sizeof(char *) * CONTROL_ARRAY_SIZE * block_multiple);
				valores_arr = realloc(valores_arr, sizeof(char *) * CONTROL_ARRAY_SIZE * block_multiple);
				block_multiple++;
			}

			reemplazarCarater(buf,'\n', 0);

			//split de 
			char *clave = strtok(buf,":");
			char *valor = strtok(NULL,":");
//#ifdef DEBUG
//			printf("%s %s\n", clave, valor);
//#endif

			//Llemanos la tabla
			put(tabla, strdup(clave), strdup(valor));

			//control, crear copia de string
			//Si clave se repite
			asignarValorControl(clave,valor);
			

			//Despues de procesar la linea, borramos buffer
			memset(buf,0, LINE_SIZE);
			i++;
		}

		//Sacamos TEST_ITEMS al azar
		int *numeros = numerosAzar(0, num_elementos_arr, TEST_ITEMS);

		int z = 0;
		for(z = 0 ; z < TEST_ITEMS; z++){
			int idx = numeros[z];			//Spectre...
			char *clave_control = claves_arr[idx];
			char *valor = (char*)get(tabla, clave_control);		///buscamos esta clave

			if(valor != NULL){
				if(strcmp(valor, valores_arr[idx]) != 0){
					printf("ERROR: la clave %s deberia tener el valor %s, pero se encontro %s\n", clave_control, valores_arr[idx], valor);
				}
			}
			else{
				printf("ERROR: get() devolvio NULL para la clave %s\n", clave_control);
			}

		}
#ifdef DEBUG

		//Solo para verificar la copia
		mostrarClavesValoresControl();
#endif

	}
	else{
		printf("ERROR: No se pudo abrir el archivo de prueba");
	}

	return;

}

void pruebas(hashtable *tabla, char *archivo){

	claves_arr = malloc(sizeof(char*) * CONTROL_ARRAY_SIZE);
	valores_arr = malloc(sizeof(char*) * CONTROL_ARRAY_SIZE);
	num_elementos_arr = 0;

	//Prueba 1 y 2. llenar tabla con datos prueba put/get
	printf("\n******************** PRUEBA 1 y 2: LLENAR TABLA Y PUT/GET ********************\n");
	llenarTabla(tabla, archivo);


	//Prueba 3. Obtener numero de elementos
	printf("\n******************** PRUEBA 3: NUMERO DE ELEMENTOS ********************\n");
	int num_elem = numeroElementos(tabla);
	
	if(num_elem != num_elementos_arr){
		printf("ERROR: el numero de elementos de la tabla reportado por la hashtabla es incorrecto\n");
	}

	
	//Prueba 4. Verificar si existe clave y no
	printf("\n******************** PRUEBA 4: EXISTENCIA DE CLAVE ********************\n");
	int controlIdx = numeroAzar(0, num_elementos_arr);
	
	if(contieneClave(tabla, claves_arr[controlIdx]) == 0){
		printf("ERROR: clave %s deberia existir\n", claves_arr[controlIdx]);
	}


	//Prueba 5. Obtener todas las claves
	printf("\n******************** PRUEBA 5: OBTENER TODAS LAS CLAVES ********************\n");

	int cant = 0;
	char ** lista_claves = claves(tabla, &cant);
	int res = verificarExistenciaClaves(lista_claves, cant);

	if(res > 0){
		printf("ERROR: claves() devolvio mas claves de las que se insertaron\n");
	}
	else if(res < 0){
		printf("ERROR: claves() devolvio menos claves de las que se insertaron\n");
	}

	//Prueba 6. Obtener todos los valores
	printf("\n******************** PRUEBA 6: OBTENER TODOS LOS VALORES ********************\n");

	cant = 0;
	char ** lista_valores = (char **)valores(tabla, &cant);
	res = verificarExistenciaValores(lista_valores, lista_claves, cant);

	if(res > 0){
		printf("ERROR: valores() devolvio mas claves de las que se insertaron\n");
	}
	else if(res < 0){
		printf("ERROR: valores() devolvio menos claves de las que se insertaron\n");
	}

		//TODO: verificar que maneje NULL;
	//Prueba 7. Remover una clave
	printf("\n******************** PRUEBA 7: REMOVER CLAVE ********************\n");

	char *removido = (char *)remover(tabla,claves_arr[controlIdx]);
	if(removido == NULL){
		printf("ERROR: El elemento removido fue NULL\n");
	}
	else{
		printf("El elemento removido es %s : %s\n", claves_arr[controlIdx], removido);
	}
	if(contieneClave(tabla, claves_arr[controlIdx]) == 1){
		printf("ERROR: clave %s NO deberia existir\n", claves_arr[controlIdx]);
	}

	//Prueba 8. Eliminar todos los elementos
	printf("\n******************** PRUEBA 8: ELIMINAR TODOS LOS ELEMENTOS ********************\n");

	int cant1 = -1;
	int cant2 = -1;
	borrar(tabla);
	lista_claves = claves(tabla, &cant1);
	lista_valores = (char **)valores(tabla, &cant2);

	if(cant1 != 0 || cant2 !=0){
		printf("ERROR no se ha borrado correctamente la tabla");
	}



	return;

}


int main(int argc, char** argv){

	int i = 0;
	for(i = 0; i < argc; i++){
		if(strcmp("-h", argv[i]) == 0){
			printf("Uso: ./prueba numero_buckets ruta_archivo numero_items_test\n");
			printf("El maximo valor de numero_items_test deber ser la cantidad de elementos en el archivo de prueba - 1\n");
			exit(0);
		}
	}

	int num_buckets;
	char *archivo;
	if(argc < 4){
		printf("Numero insuficiente de argumentos. Agregue el argumento -h para ver el uso este programa\n");
		exit(-1);
	}
	else{
		char *num_buckets_str = argv[1];
		num_buckets = atoi(num_buckets_str);
		archivo = argv[2];
		TEST_ITEMS = atoi( argv[3]); 
	}

	hashtable *tabla = crearHashTable(num_buckets); 

	if(tabla != NULL){
		pruebas(tabla, archivo);
	}
	else{
		printf("ERROR: la tabla no se puedo crear (NULL)");
	}

	


	return 0;

}





