#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

void put(hashtable *tabla, char *clave, void *valor){
	int index= (long int)((hash((unsigned char *)clave))%(tabla->numeroBuckets));
		
	
	objeto *actual;
	actual=(tabla->buckets)[index];
	while (actual!= NULL && actual->siguiente!=NULL && strcmp(clave, actual->clave)!=0){ //Se recorre los elementos del bucket hasta encontrar uno vacio
		actual=actual->siguiente;
		
	}
	if(actual!= NULL && strcmp(clave, actual->clave)==0){ //Si la clave ya existe se cambia su valor
		actual->valor=NULL;
		actual->valor=valor;
	}
	else{
		objeto *nuevoObj= malloc(sizeof(objeto));
		if (nuevoObj==NULL){  
			return;
		}
		
		nuevoObj->clave=clave;
		nuevoObj->valor=valor;
		nuevoObj->siguiente=NULL;
		
		if(actual==NULL){ //se inserta al inicio del bucket
			(tabla->buckets)[index]=nuevoObj;
			tabla->elementos++;
		}
		else if(actual->siguiente== NULL){//se inserta al final del bucket
			actual->siguiente=nuevoObj;
			tabla->elementos++;
		}
		
		
	}
}


