#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

char **claves(hashtable *tabla, int *conteo){
	char **keys= (char**) malloc(sizeof(char *)*tabla->elementos); //Se crea el arreglo que devolverá el método.
	int tamano= tabla->numeroBuckets;
	int cont=0;
	for(int i=0;i<tamano;i++){	//Se recorre cada bucket.
		objeto *o;
		o=(tabla->buckets)[i]; //Se selecciona el bucket para pasar a recorrer la lista de objetos.
		while(o!=NULL){
			keys[cont]=o->clave;
			o=o->siguiente;
			cont++; //El contador permitirá recorrer el arreglo local para insertar las claves dentro.
		}
	}
	*conteo= cont; //El valor de conteo además será lo que haya resultado de todo el recorrido de claves, que se manejó en la variable cont.
	return keys;
}
