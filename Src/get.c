#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

void *get(hashtable *tabla, char *clave){
	int index= (long int)((hash((unsigned char *)clave))%(tabla->numeroBuckets)); //Se busca el indice utilizando la función de hashing.
	objeto *o=(tabla->buckets)[index]; //Se crea un puntero de tipo objeto que apunte al bucket en el índice resultante.
	while(o!=NULL){ //La lista de objetos termina cuando un elemento apunta a NULL. Si el bucket está vacío, no entra al while porque el objeto es NULL.
		if(strcmp(o->clave,clave)==0){
			return o->valor;
		}
		o=o->siguiente; //En el recorrido del while, si la clave del objeto no es igual a lo buscado, se pasa al siguiente objeto.
	}
	return NULL; //Cuando no entra al while, se retorna NULL por lo antes mencionado.
}
