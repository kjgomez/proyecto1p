#Escriba su makefiile en este archivo
#
# Kevin Gomez
# Pedro Mendoza

prueba: crearHashTable.o numeroElementos.o put.o get.o remover.o borrar.o claves.o contieneClave.o valores.o hash.o prueba.o
	gcc Obj/crearHashTable.o Obj/numeroElementos.o Obj/put.o Obj/get.o Obj/remover.o Obj/borrar.o Obj/claves.o Obj/contieneClave.o Obj/valores.o Obj/hash.o Obj/prueba.o -o Bin/prueba

crearHashTable.o: Src/crearHashTable.c 
	gcc -Wall -c Src/crearHashTable.c -o Obj/crearHashTable.o -I include/ -g
numeroElementos.o: Src/borrar.c
	gcc -Wall -c Src/numeroElementos.c -o Obj/numeroElementos.o -I include/ -g
put.o: Src/put.c 
	gcc -Wall -c Src/put.c -o Obj/put.o -I include/ -g
get.o: Src/get.c
	gcc -Wall -c Src/get.c -o Obj/get.o -I include/ -g
remover.o: Src/remover.c
	gcc -Wall -c Src/remover.c -o Obj/remover.o -I include/ -g
borrar.o: Src/borrar.c
	gcc -Wall -c Src/borrar.c -o Obj/borrar.o -I include/ -g
claves.o: Src/claves.c
	gcc -Wall -c Src/claves.c -o Obj/claves.o -I include/ -g
contieneClave.o: Src/contieneClave.c 
	gcc -Wall -c Src/contieneClave.c -o Obj/contieneClave.o -I include/ -g
valores.o: Src/valores.c
	gcc -Wall -c Src/valores.c -o Obj/valores.o -I include/ -g
hash.o: Src/hash.c
	gcc -Wall -c Src/hash.c -o Obj/hash.o -I include/ -g
prueba.o: Src/prueba.c
	gcc -Wall -c Src/prueba.c -o Obj/prueba.o -I include/ -g
.PHONY: clean
clean:
	rm Obj/prueba.o Obj/crearHashTable.o Obj/borrar.o Obj/claves.o Obj/contieneClave.o Obj/get.o Obj/numeroElementos.o Obj/put.o Obj/remover.o Obj/valores.o Obj/hash.o Bin/prueba
