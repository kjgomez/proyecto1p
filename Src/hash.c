#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"


unsigned long hash(unsigned char *str){
	unsigned long hash = 5381;
	int c;
	c=*str++;
	while (c){
		hash = ((hash << 5) + hash) + c;
		c=*str++;
	}
	return hash;
}
